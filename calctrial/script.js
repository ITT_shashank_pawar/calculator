const numberButtons = document.querySelectorAll('.numbers');
const operationButtons = document.querySelectorAll('.operations');
const equalsButton = document.querySelector('.equals');
const clearButton = document.querySelector('.clear');
const deleteButton = document.querySelector('.delete');
let previousData = document.querySelector('.previousOperand');
let currentData = document.querySelector('.currentOperand');

let disp1 = ''
let disp2 = ''
let operation = ''
let value = 0


numberButtons.forEach(button => {
    button.addEventListener('click', (e) => {
        disp2 = disp2 + e.target.innerText;
        currentData.innerText = disp2;
    })
}
)

operationButtons.forEach(button => {
    button.addEventListener('click', (e) => {
        operation = e.target.innerText
         disp1 = currentData.innerText
        if (disp2 === '') return
        if ((disp1 !== '') && (operation)) {
            value = parseFloat(disp2)
            disp1 = disp2 + operation

            previousData.innerText = disp1
            disp2 = ''
             currentData.innerText = ''
        }
    })
})



clearButton.addEventListener('click', () => {
    currentData.innerText = 0
    previousData.innerText = 0
    disp1 = ''
    disp2 = ''
    operation = ''
    value = 0
})

deleteButton.addEventListener('click', () => {
    currentData.innerText = ''
})

equalsButton.addEventListener('click', () => {
    disp1 += disp2                                         
    previousData.innerText = disp1                            

    value = computation(operation);
    currentData.innerText = value
    disp2 = value
})

function computation(operation) {
    if (operation === '+') {
        value = value + parseFloat(disp2);
    }
    else if (operation === '-')
        value = value - parseFloat(disp2);
    else if (operation === '*')
        value = value * parseFloat(disp2);
    else if (operation === '/')
        value = value / parseFloat(disp2);
    else if (operation === '%')
        value = value % parseFloat(disp2);

    return value

}

